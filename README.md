# Bluesky - bidouilles & co

Ici quelques essais / trucs / bricolages autours de l'API [Bluesky](https://bsky.app/), et plus particuliérement autour de [AT Protocol](https://atproto.com/)  
Note : c'est de la bidouille, et créé sans grandes prétentions, juste pour partager, les remarques constructives sont les bienvenues, les PR encore plus 😉

>  🖥 [Rubrique BASH & Curl](./bash/README.md) 

C'est toujours un de mes points de départ pour tester, avant de passer sur un langage + structuré

- [Références](./bash/README.md#références)
- [pré-requis](./bash/README.md#pré-requis)
- [Récupérer son DID à partir de son Handle](./bash/README.md#récupérer-son-did-à-partir-de-son-handle)
- [Récupérer un token d'identification](./bash/README.md#récupérer-un-token-didentification)
- [Requête de lecture](./bash/README.md#requête-de-lecture)
- - [Exemple 1: récupérer le contenu de sa timeline](./bash/README.md#exemple-1-récupérer-le-contenu-de-sa-timeline)
- - [Exemple 2: récupérer la liste de ses followers](./bash/README.md#exemple-2-récupérer-la-liste-de-ses-followers)
- - [Exemple 3: Récupérer le nombre de notifications](./bash/README.md#exemple-3-récupérer-le-nombre-de-notifications)
- [Requête d'écriture](./bash/README.md#requête-décriture)
- - [Exemple 4: Publier un message simple](./bash/README.md#exemple-4-publier-un-message-simple)

> 🧦 [Rubrique Websocket](./wss/README.md)  

Vive le streaming 😎 
