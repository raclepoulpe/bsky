# websocket bsky

## Références

- [AT Protocol com.atproto.sync.subscribeRepos](https://atproto.com/lexicons/com-atproto-sync#comatprotosyncsubscriberepos)
- [AT Protocol Event stream](https://atproto.com/specs/event-stream)
- [Bluesky Firehose example](https://gitlab.com/-/snippets/2540995)
- [Deno - runtime js](https://deno.com/)

## Tests

Fichier bs-firehose.ts :  
```typescript
import { decodeMultiple } from 'https://deno.land/x/cbor@v1.5.2/index.js';

/** Open a firehose to Bluesky, getting a record of every event in realtime. */
function openFirehose(
  service = 'wss://bsky.social',
  onMessage: (message: any) => void,
) {
  const ws = new WebSocket(`${service}/xrpc/com.atproto.sync.subscribeRepos`);
  ws.binaryType = 'arraybuffer';

  ws.addEventListener('message', ({ data }: MessageEvent<ArrayBuffer>) => {
    const message = decodeMultiple(new Uint8Array(data));
    onMessage(message);
  });
}

openFirehose('wss://bsky.social', console.log);
```

Connexion au firehose bluesky via websocket :  

```bash
cat bs-firehose.ts | deno run -A -
```

Flux en sortie console :  

```json
[
  { t: "#commit", op: 1 },
  {
    ops: [
      {
        cid: null,
        path: "app.bsky.graph.follow/3k5lntzctlf2d",
        action: "delete"
      }
    ],
    seq: 190610111,
    prev: Tag {
      value: Uint8Array(37) [
          0,   1, 113,  18,  32,  58,  43, 232,  17,
        160, 195,  64,  85, 141, 197, 136,  32,   7,
         50, 209, 213, 165, 183, 212, 159, 100, 201,
         59, 182,  65,  74,  65, 189,  97, 230,  56,
          8
      ],
      tag: 42
    },
    repo: "did:plc:mgftsovindbralcmhen55jpy",
    time: "2023-08-24T07:59:28.076Z",
    blobs: [],
    blocks: Uint8Array(3084) [
       58, 162, 101, 114, 111, 111, 116, 115, 129, 216,  42,  88,
       37,   0,   1, 113,  18,  32,  11,  37,   0,  28, 105, 224,
      170, 187, 198,  39, 182, 179,  33,  24, 123,  50,  90, 177,
      192,  73, 148,  74,   2, 209,  21, 146, 107, 125,  20, 136,
      153, 254, 103, 118, 101, 114, 115, 105, 111, 110,   1, 189,
        2,   1, 113,  18,  32, 149, 114, 224,  82,  78, 130,  71,
      100,   5, 167, 240, 203,  70, 162, 209, 214, 140, 169, 241,
      240, 139,  56,   4, 109, 240, 151, 244, 243,  81, 143,  33,
      211, 162,  97, 101,
      ... 2984 more items
    ],
    commit: Tag {
      value: Uint8Array(37) [
          0,   1, 113,  18,  32,  11,  37,   0,  28,
        105, 224, 170, 187, 198,  39, 182, 179,  33,
         24, 123,  50,  90, 177, 192,  73, 148,  74,
          2, 209,  21, 146, 107, 125,  20, 136, 153,
        254
      ],
      tag: 42
    },
    rebase: false,
    tooBig: false
  }
]
```
