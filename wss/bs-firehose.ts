import { decodeMultiple } from 'https://deno.land/x/cbor@v1.5.2/index.js';

/** Open a firehose to Bluesky, getting a record of every event in realtime. */
function openFirehose(
  service = 'wss://bsky.social',
  onMessage: (message: any) => void,
) {
  const ws = new WebSocket(`${service}/xrpc/com.atproto.sync.subscribeRepos`);
  ws.binaryType = 'arraybuffer';
  
  ws.addEventListener('message', ({ data }: MessageEvent<ArrayBuffer>) => {
    const message = decodeMultiple(new Uint8Array(data));
    onMessage(message);
  });
}

openFirehose('wss://bsky.social', console.log);

